<?php


class User
{

    public static function register($name, $phone, $email, $password){
        $db = Db::getConnection();

        $password = password_hash($password, PASSWORD_DEFAULT);

        $sql = "INSERT INTO users (login, email, phone, password) VALUES (:name, :email, :phone, :password)";

        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':phone', $phone, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);

        $result->execute();
        /*print_r($result->errorInfo());*/
        return $result;
    }

    public static function editData($id, $name, $email, $phone)
    {
        $db = Db::getConnection();

        $sql = 'UPDATE users SET login = :name, phone = :phone, email = :email WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':phone', $phone, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function editPassword($id, $password)
    {
        $db = Db::getConnection();

        $password = password_hash($password, PASSWORD_DEFAULT);

        $sql = 'UPDATE users SET password = :password WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function checkUserData($email)
    {
        $db = Db::getConnection();


        $sql = 'SELECT * FROM users WHERE email = :email';

        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $user = $result->fetch();
        return $user;
    }

    public static function CheckUserPassword($userId)
    {
        $db = Db::getConnection();

        $sql = 'SELECT password FROM users WHERE id = :userId';

        $result = $db->prepare($sql);
        $result->bindParam(':userId', $userId, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $user = $result->fetch();
        if($user){
            return $user;
        }
        return false;
    }

    public static function getUserById($id)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM users WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        return $result->fetch();
    }

    public static function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }

    public static function checkLogged()
    {
        if(isset($_SESSION['user'])){
            return $_SESSION['user'];
        }
        header("Location: /user/login");
    }

    public static function isGuest()
    {
        if(isset($_SESSION['user'])){
            return false;
        }
        return true;
    }

    public static function checkName($name)
    {
        if(strlen($name) >= 3){
            return true;
        }
        return false;
    }

    public static function checkEmail($email)
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }
        return false;
    }

    public static function checkPhone($phone)
    {
        if (strlen($phone) >= 10) {
            return true;
        }
        return false;
    }

    public static function checkPassword($password)
    {
        if(strlen($password) >= 6){
            return true;
        }
        return false;
    }

    public static function checkEmailExist($email)
    {
        $db = Db::getConnection();

        $sql = 'SELECT count(*) FROM users WHERE email = :email';

        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if($result->fetchColumn()){
            return true;
        }
        return false;
    }

    public static function checkNameExist($name)
    {
        $db = Db::getConnection();

        $sql = 'SELECT count(*) FROM users WHERE login = :name';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->execute();

        if($result->fetchColumn()){
            return true;
        }
        return false;
    }

}