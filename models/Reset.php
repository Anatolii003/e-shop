<?php


class Reset
{

    public static function checkUserEmail($email)
    {
        $db = Db::getConnection();

        $sql = 'SELECT email, login FROM users WHERE email = :email';

        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $userEmail = $result->fetch();
        return $userEmail;
    }

    public static function editCode($code, $email)
    {
        $db = Db::getConnection();

        $sql = 'UPDATE users SET code = :code WHERE email = :email';
        $result = $db->prepare($sql);
        $result->bindParam(':code', $code, PDO::PARAM_INT);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
        return $result;
    }

    public static function checkUser($login, $code)
    {
        $db = Db::getConnection();

        $sql = 'SELECT id FROM users WHERE login = :login AND code = :code';

        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':code', $code, PDO::PARAM_STR);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result = $result->fetch();
        return $result;
    }

    public static function editPassword($password, $login, $code)
    {
        $db = Db::getConnection();
        $password = password_hash($password, PASSWORD_DEFAULT);

        $sql = 'UPDATE users SET password = :password WHERE login = :login AND code = :code';
        $result = $db->prepare($sql);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':code', $code, PDO::PARAM_STR);
        return $result->execute();
    }

}