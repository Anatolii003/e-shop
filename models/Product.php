<?php

/**
 * Class Product
 */
class Product
{

    const COUNTS_IN_CATEGORY = 3;
    const COUNTS_IN_CATALOG = 6;

    /**
     * @param int $count
     * @return array
     */
    public static function getLatestProductHome($count = self::COUNTS_IN_CATALOG)
    {
        $count = intval($count);

        $db = Db::getConnection();

        $productList = array();

        $result = $db->query('SELECT id, name, price, is_new, availability FROM product WHERE status = 1 ORDER BY id DESC LIMIT ' . $count);

        $i=0;
        while ($row = $result->fetch()){
            $productList[$i]['id'] = $row['id'];
            $productList[$i]['name'] = $row['name'];
            $productList[$i]['price'] = $row['price'];
            $productList[$i]['is_new'] = $row['is_new'];
            $productList[$i]['availability'] = $row['availability'];
            $i++;
        }

        return $productList;

    }

    /**
     * @param int $page
     * @return array
     */
    public static function getLatestProductCatalog($page = 1)
    {
        $count = self::COUNTS_IN_CATALOG;
        $count = intval($count);


        $page = intval($page);
        $offset = ($page - 1) * $count;

        $db = Db::getConnection();

        $productList = array();

        $result = $db->query('SELECT id, name, price, is_new, availability FROM product WHERE status = 1 ORDER BY id DESC LIMIT ' . $count . " OFFSET " . $offset);

        $i=0;
        while ($row = $result->fetch()){
            $productList[$i]['id'] = $row['id'];
            $productList[$i]['name'] = $row['name'];
            $productList[$i]['price'] = $row['price'];
            $productList[$i]['is_new'] = $row['is_new'];
            $productList[$i]['availability'] = $row['availability'];
            $i++;
        }

        return $productList;

    }

    /**
     * @param bool $categoryId
     * @param int $page
     * @return array
     */
    public static function getProductListByCategory($categoryId = false, $page = 1)
    {

        if($categoryId){

            $page = intval($page);
            $offset = ($page - 1) * self::COUNTS_IN_CATEGORY;

            $db = Db::getConnection();

            $product = array();

            $result = $db->query("SELECT id, name, price, is_new, availability FROM product WHERE status = '1' AND category_id = '$categoryId' ORDER BY id DESC LIMIT " . self::COUNTS_IN_CATEGORY . " OFFSET " . $offset);

            $i = 0;
            while($row = $result->fetch()){
                $product[$i]['id'] = $row['id'];
                $product[$i]['name'] = $row['name'];
                $product[$i]['price'] = $row['price'];
                $product[$i]['is_new'] = $row['is_new'];
                $product[$i]['availability'] = $row['availability'];
                $i++;
            }
            return $product;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getProductById($id)
    {
        $id = intval($id);
        if($id){
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM product WHERE id = ' . $id);

            $result->setFetchMode(PDO::FETCH_ASSOC);

            return $result->fetch();
        }
    }

    /**
     * @param $idsArray
     * @return array
     */
    public static function getProductsByIds($idsArray)
    {
        $products = array();
        $db = Db::getConnection();
        $idsString = implode(',', $idsArray);

        $result = $db->query("SELECT * FROM product WHERE status = '1' AND id IN($idsString)");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $i = 0;
        while($row = $result->fetch()){
            $products[$i]['id'] = $row['id'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $i++;
        }
        return $products;
    }

    public static function getRecommendedProducts()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT id, name, price, is_new FROM product '
            . 'WHERE status = "1" AND is_recommended = "1" '
            . 'ORDER BY id DESC');
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        return $productsList;
    }

    public static function getProductsList()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT id, name, price, code FROM product ORDER BY id ASC');
        $productsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['code'] = $row['code'];
            $productsList[$i]['price'] = $row['price'];
            $i++;
        }
        return $productsList;
    }

    /**
     * @param $categoryId
     * @return mixed
     */
    public static function getTotalProductsInCategory($categoryId)
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM product WHERE status = "1" AND category_id = ' . $categoryId);

        $row = $result->fetch();

        return $row['count'];
    }

    /**
     * @return mixed
     */
    public static function getTotalProductsInCatalog()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM product WHERE status = "1"');

        $row = $result->fetch();

        return $row['count'];
    }

    public static function getImage($id)
    {
        $noImage = 'no-image.jpg';

        $path = '/upload/images/products/';

        $pathToProductImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            return $pathToProductImage;
        }

        return $path . $noImage;
    }

    public static function createProduct($options)
    {
        $db = Db::getConnection();

        $sql = 'INSERT INTO product '
            . '(name, code, price, category_id, brand, availability,'
            . 'description, is_new, is_recommended, status)'
            . 'VALUES '
            . '(:name, :code, :price, :category_id, :brand, :availability,'
            . ':description, :is_new, :is_recommended, :status)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function updateProductById($id, $options)
    {
        $db = Db::getConnection();

        $sql = "UPDATE product
            SET 
                name = :name, 
                code = :code, 
                price = :price, 
                category_id = :category_id, 
                brand = :brand, 
                availability = :availability, 
                description = :description, 
                is_new = :is_new, 
                is_recommended = :is_recommended, 
                status = :status
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function deleteProductById($id)
    {
        $db = Db::getConnection();

        $sql = 'DELETE FROM product WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

}