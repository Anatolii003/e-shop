<?php


class Utf8mail
{

    public static function sendUtf8mail($to, $s, $body)
    {
        $from_name = "Назва інтернет сайту";
        $from_a = "test@mail.ru";
        $reply = "test@mail.ru";
        $s = "=?utf-8?b?" . base64_encode($s) . "?=";
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "From: =?utf-8?b?" . base64_encode($from_name) . "?= <" . $from_a . ">\r\n";
        $headers .= "Content-Type: text/plain;charset=utf-8\r\n";
        $headers .= "Reply-To: $reply\r\n";
        $headers .= "X-Mailer: PHP/" . phpversion();
        mail($to, $s, $body, $headers);
        return true;
    }

}