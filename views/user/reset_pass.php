<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <div class="col-sm-4 col-sm-offset-4 padding-right">
                    <?php if ($result): ?>
                        <p>Ваш пароль змінено</p>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>


                        <div class="signup-form"><!--sign up form-->
                            <h2>Відновлення паролю. Введіть логін та код відправлені Вам на електронну пошту</h2>
                            <form action="#" method="post">
                                <p>Ваш логин</p>
                                <input type="text" name="name" placeholder="Ваш логин"/>
                                <p>Ваш код</p>
                                <input type="text" name="code" placeholder="Ваш код"/>
                                <p>Новий пароль</p>
                                <input type="password" name="password" placeholder="Новий пароль"/>
                                <input type="submit" name="submit" class="btn btn-default" value="Відновити пароль"/>
                            </form>
                        </div><!--/sign up form-->

                    <?php endif; ?>
                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>