<?php require_once ROOT . '/views/layouts/header.php' ?>

    <section id="form"><!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 padding-right">
                    <?php if ($result): ?>
                        <p>Ви зареєстровані</p>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <div class="signup-form"><!--sign up form-->
                            <h4>Введіть свої дані будь ласка!</h4>
                            <form action="#" method="post">
                                <p>Ім'я</p>
                                <input type="text" name="name" placeholder="Введіть ім'я" value="<?php echo $name; ?>"/>
                                <p>Email</p>
                                <input type="email" name="email" placeholder="Введіть email"
                                       value="<?php echo $email; ?>"/>
                                <p>Телефон</p>
                                <input type="text" name="phone" placeholder="Введіть номер телефону"
                                       value="<?php echo $phone; ?>"/>
                                <p>Пароль</p>
                                <div class="password">
                                    <input type="password" id="password-input" name="password" placeholder="Введіть пароль" value="<?php echo $password; ?>"/>
                                    <a href="#" class="password-control"></a>
                                </div>
                                <input type="submit" name="submit" class="btn btn-default" value="Зареєструватися"/>
                            </form>


                        </div><!--/sign up form-->
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section><!--/form-->


<?php require_once ROOT . '/views/layouts/footer.php' ?>