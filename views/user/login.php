<?php require_once ROOT . '/views/layouts/header.php' ?>

    <section id="form"><!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-4 padding-right">


                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <div class="signup-form"><!--sign up form-->
                            <h2>Вхід на сайт</h2>
                            <form action="#" method="post">
                                <p>Email</p>
                                <input type="email" name="email" placeholder="Email Address"
                                       value="<?php echo $email; ?>"/>
                                <p>Пароль</p>
                                <div class="password">
                                    <input type="password" id="password-input" name="password" placeholder="Password" value="<?php echo $password; ?>"/>
                                    <a href="#" class="password-control"></a>
                                </div>
                                <input type="submit" name="submit" class="btn btn-default" value="Увійти"/>
                            </form>
                            <a href="/user/register">Зареєструватися</a><br>
                            <a href="/user/reset">Забули пароль?</a>
                        </div><!--/sign up form-->

                </div>
            </div>
        </div>
    </section><!--/form-->


<?php require_once ROOT . '/views/layouts/footer.php' ?>