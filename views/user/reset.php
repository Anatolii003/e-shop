<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <div class="col-sm-4 col-sm-offset-4 padding-right">

                    <?php if ($result): ?>
                        <p>Вам на пошту відправлено код скидання та логін. Перевірте пошту і <a href="/user/resetPass">перейдіть за цією адресою</a></p>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <div class="signup-form"><!--sign up form-->
                            <h2>Відновити пароль</h2>
                            <form action="#" method="post">
                                <p>Ваш email:</p>
                                <input type="email" name="email" placeholder="Email"/>

                                <input type="submit" name="submit" class="btn btn-default" value="Відправити"/>
                            </form>
                        </div><!--/sign up form-->
                    <?php endif; ?>

                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>