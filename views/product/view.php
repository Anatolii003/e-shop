<?php include ROOT . '/views/layouts/header.php'?>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="left-sidebar">
                            <h2>Каталог</h2>
                            <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                                <?php foreach($categoryList as $category):?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a href="/category/<?php echo $category['id']?>" ><?php echo $category['name']?></a></h4>
                                    </div>
                                </div>
                                <?php endforeach;?>
                            </div><!--/category-products-->

                        </div>
                    </div>


                    <div class="col-sm-9 padding-right">
                        <div class="product-details"><!--product-details-->
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="view-product">
                                        <img src="<?php echo Product::getImage($productId['id']); ?>" alt=""/>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="product-information"><!--/product-information-->
                                        <?php if ($productId['is_new']): ?>
                                            <img src="/template/images/product-details/new.jpg" class="newarrival"
                                                 alt=""/>
                                        <?php endif; ?>

                                        <h2><?php echo $productId['name']; ?></h2>
                                        <p>Код товару: <?php echo $productId['code']; ?></p>
                                        <span>
                                            <span><?php echo $productId['price']; ?> $</span>
                                            <label>Кількість:</label>
                                            <input type="text" value="3"/>
                                            <button type="button" class="btn btn-fefault cart">
                                                <i class="fa fa-shopping-cart"></i>
                                                Купити
                                            </button>
                                        </span>

                                        <p><b>Наявність:</b>
                                            <?php
                                            if ($productId['availability']) {
                                                echo 'На складі';
                                            } else echo 'Немає на складі';
                                            ?>
                                        </p>

                                        <p><b>Стан:</b> Нове</p>
                                        <p><b>Виробник:</b> <?php echo $productId['brand']; ?></p>
                                    </div><!--/product-information-->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>Опис товару</h5>
                                    <?php echo $productId['description'] ?>
                                </div>
                            </div>
                        </div><!--/product-details-->

                    </div>

                </div>
            </div>
        </section>


        <br/>
        <br/>

<?php include ROOT . '/views/layouts/footer.php'?>