<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <div class="col-sm-4 col-sm-offset-4 padding-right">

                    <?php if ($result): ?>
                        <h2>Дані змінено!</h2>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li> - <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <div class="signup-form"><!--sign up form-->
                            <h2>Редагування даних</h2>
                            <form action="#" method="post">
                                <p>Ваш пароль:</p>
                                <div  class="password">
                                <input type="password" id="password-input" name="password" placeholder="Ваш пароль"/>
                                    <a href="#" class="password-control"></a>
                                </div>
                                <p>Новий пароль:</p>
                                    <div  class="password__new">
                                <input type="password" id="password-input__new" name="new_password" placeholder="Новий пароль"/>
                                        <a href="#" class="password-control__new"></a>
                                    </div>
                                <br/>
                                <input type="submit" name="submit" class="btn btn-default" value="Зберегти" />
                            </form>
                        </div><!--/sign up form-->


                    <?php endif; ?>
                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>