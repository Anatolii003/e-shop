<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <h1>Кабінет користувача</h1>

                <h3>Привіт, <?php echo $user['login']; ?>!</h3>
                <ul>
                    <li><a href="/cabinet/edit">Редагувати дані</a></li>
                    <li><a href="/cabinet/editPassword">Змінити пароль</a></li>
                    <li><a href="/cabinet/history">Список покупок</a></li>
                    <?php if ($user['role'] == 'admin'): ?>
                        <li><a href="/admin/">В адмінпанель</a></li>
                    <?php endif ?>
                </ul>

            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>