<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <h1>Кабінет користувача</h1>

                <h3>Привіт, <?php echo $user['login']; ?>!</h3>

                <div class="col-sm-9 padding-right">
                    <div class="features_items">
                        <h2 class="title text-center">Історія ваших покупок</h2>

                        <?php if ($results): ?>
                            <?php foreach ($results as $result): ?>
                                <caption><?php echo $result['order_date']; ?></caption>
                                <table class="table-bordered table-striped table">
                                    <tr>
                                        <th>Код товару</th>
                                        <th>Назва товару</th>
                                        <th>Вартість, $</th>
                                        <th>Кількість, шт</th>
                                    </tr>
                                    <?php foreach ($result['order_products'] as $orderProducts): ?>
                                            <tr>
                                                <td><?php echo $orderProducts['code']; ?></td>
                                                <td>
                                                    <a href="/product/<?php echo $orderProducts['id']; ?>">
                                                        <?php echo $orderProducts['name']; ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $orderProducts['price']; ?>$</td>

                                                <td><?php echo $result['order_count'][$orderProducts['id']]; ?></td>


                                            </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <td colspan="2">Загальна вартість:</td>
                                        <td colspan="2"><?php echo $result['total_price']; ?>$</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Статус:</td>
                                        <td><?php echo Order::getStatusText($result['order_status']); ?></td>
                                    </tr>

                                </table>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>

                </div>

            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>