<?php

/**
 * Class AdminController
 * Extends AdminBase
 * Main page of the administrative panel
 */
class AdminController extends AdminBase
{

    /**
     * action main page of the administrative panel
     * @return bool
     */
    public function actionIndex()
    {
        self::checkAdmin();

        require_once ROOT . '/views/admin/index.php';
        return true;
    }

}