<?php

/**
 * Class ProductController
 * Product action
 */
class ProductController
{

    /**
     * action view product page
     * @param string $id - Product ID
     * @var array $categoriesList - Categories array
     * @var array $productId - Array of product information
     * @return bool
     */
    public function actionView($id)
    {
        $categoryList = array();
        $categoryList = Category::getCategoryList();

        $productId = Product::getProductById($id);

        require_once (ROOT . '/views/product/view.php');
        return true;
    }

}