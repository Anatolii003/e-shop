<?php

/**
 * Class CartController
 * actions add and remove product in the cart, cart page, checkout goods page
 */
class CartController
{

    /**
     * action add product to cart
     * @param string $id - Product ID
     * @return bool
     */
    public function actionAddAjax($id)
    {
        echo Cart::addProduct($id);
        return true;
    }

    /**
     * action remove product from the cart
     * @param string $id - Product ID
     */
    public function actionDelete($id)
    {
        Cart::deleteProduct($id);

        header("Location: /cart");
    }

    /**
     * action cart page
     * @var array $categories - Categories array
     * @var array $productsInCart - Products ID in cart from session
     * @var array $productsIds - Products ID
     * @var array $products - Array of information about goods from the cart
     * @var integer $totalPrice - Total price of goods from the cart
     * @return bool
     */
    public function actionIndex()
    {
        $categories = array();
        $categories = Category::getCategoryList();

        $productsInCart = false;
        $productsInCart = Cart::getProducts();

        if ($productsInCart) {
            $productsIds = array_keys($productsInCart);
            $products = Product::getProductsByIds($productsIds);

            $totalPrice = Cart::getTotalPrice($products);
        }

        require_once(ROOT . '/views/cart/index.php');
        return true;
    }

    /**
     * action checkout goods
     * @var array $categories - Categories array
     * @var $userName - User name
     * @var $userEmail - User email
     * @var $userPhone - User phone
     * @var $userComment - User comment. Additional Information
     * @var $errors - Error container
     * @var string $goods - Order information for user
     * @var $productsInCart - Products ID in cart from session
     * @var $productsIds - Products ID
     * @var $products - Array of information about goods from the cart
     * @var $totalPrice - Total price of goods from the cart
     * @var $totalQuantity - Quantity of goods in the cart
     * @var boolean $result
     * @return bool
     */
    public function actionCheckout()
    {
        $categories = array();
        $categories = Category::getCategoryList();

        $result = false;

        if (isset($_POST['submit'])) {
            $userName = $_POST['userName'];
            $userEmail = $_POST['userEmail'];
            $userPhone = $_POST['userPhone'];
            $userComment = $_POST['userComment'];

            $errors = false;

            if (!User::checkName($userName)) {
                $errors[] = 'Невірне ім\'я';
            }
            if (!User::checkPhone($userPhone)) {
                $errors[] = 'Невірний телефон';
            }

            $userId = 0;
            $goods = '';
            
            $productsInCart = Cart::getProducts();
            $productsIds = array_keys($productsInCart);
            $products = Product::getProductsByIds($productsIds);
            $totalPrice = Cart::getTotalPrice($products);
            $totalQuantity = Cart::countItems();

            foreach ($products as $product) {
                $goods .= "Код: {$product['code']}, Назва: {$product['name']}, Ціна: {$product['price']}$, Кількість: {$productsInCart[$product['id']]}\r\n";
            }

            if ($errors == false) {

                if (User::isGuest()) {
                    $userId = 0;
                    if (User::checkEmailExist($userEmail)) {
                        $errors[] = 'Такий email уже існує. Якщо це ваш email то увійдіть будь ласка';
                    }
                } else {
                    $userId = User::checkLogged();
                }

                $result = Order::save($userName, $userEmail, $userPhone, $userComment, $userId, $productsInCart);
                
                if ($result) {
                    $adminEmail = 'email@domain.zone';
                    $message = "$userName Замовив:\r\n$goods \r\nНа загальну суму: $totalPrice $";
                    $subject = 'Нове замовлення!';
                    mail($adminEmail, $subject, $message);

                    Cart::clear();
                }
            }
        } else {
            $productsInCart = Cart::getProducts();

            if ($productsInCart == false) {
                header("Location: /");
            } else {
                $productsIds = array_keys($productsInCart);
                $products = Product::getProductsByIds($productsIds);
                $totalPrice = Cart::getTotalPrice($products);
                $totalQuantity = Cart::countItems();

                $userName = false;
                $userEmail = false;
                $userPhone = false;
                $userComment = false;

                if (User::isGuest()) {
                    if (User::checkEmailExist($userEmail)) {
                        $errors[] = 'Такий email уже існує. Якщо це ваш email то увійдіть будь ласка';
                    }
                } else {
                    $userId = User::checkLogged();
                    $user = User::getUserById($userId);
                    $userName = $user['login'];
                    $userEmail = $user['email'];
                    $userPhone = $user['phone'];
                }
            }
        }

        require_once(ROOT . '/views/cart/checkout.php');
        return true;
    }

}