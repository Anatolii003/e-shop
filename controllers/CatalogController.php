<?php

/**
 * Class CatalogController
 * Catalog and category actions
 */
class CatalogController
{

    /**
     * action catalog
     * @param int $page
     * @var array $categoryList - Categories array
     * @var array $latestProduct - Array of the latest products
     * @var string $total - Total amount of products in the catalog
     * @var object $pagination
     * @return bool
     */
    public function actionIndex($page = 1)
    {

        $categoryList = array();
        $categoryList = Category::getCategoryList();

        $latestProduct = array();
        $latestProduct = Product::getLatestProductCatalog($page);

        $total = Product::getTotalProductsInCatalog();

        $pagination = new Pagination($total, $page, Product::COUNTS_IN_CATALOG, 'page-');

        require_once (ROOT . '/views/catalog/index.php');

        return true;
    }

    /**
     * action category
     * @param string $categoryId
     * @param int $page
     * @var array $categoryList - Categories array
     * @var array $categoryProducts - Array of products of the corresponding category
     * @var string $total - Total amount of products in the category
     * @var object $pagination
     * @return bool
     */
    public function actionCategory($categoryId, $page = 1)
    {
        $categoryList = array();
        $categoryList = Category::getCategoryList();

        $categoryProducts = array();
        $categoryProducts = Product::getProductListByCategory($categoryId, $page);

        $total = Product::getTotalProductsInCategory($categoryId);

        $pagination = new Pagination($total, $page, Product::COUNTS_IN_CATEGORY, 'page-');

        require_once ROOT . '/views/catalog/category.php';

        return true;
    }

}