<?php

/**
 * Class CabinetController
 * Cabinet page, user data editing page, user password editing page, user shopping list page
 */
class CabinetController
{

    /**
     * action cabinet
     * @var string $userId - User ID
     * @var array $user - Array of user information
     * @return bool
     */
    public function actionIndex()
    {
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        require_once(ROOT . '/views/cabinet/index.php');
        return true;
    }

    /**
     * action user data editing
     * @var string $userId - User ID
     * @var array $user - Array of user information
     * @var string $name - User name
     * @var string $email - User email
     * @var string $phone - User phone
     * @var string $password - User password
     * @var array $errors - Error container
     * @var array $checkUser - Cached user password from database
     * @var boolean $result - Data record status
     * @return bool
     */
    public function actionEdit()
    {
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        $name = $user['login'];
        $email = $user['email'];
        $phone = $user['phone'];
        $password = false;


        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $phone = $_POST['phone'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkName($name)) {
                $errors[] = 'Ім\'я не повинно бути коротше 3-х символів';
            }

            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не повинен бути коротше 6-ти символів';
            }

            $checkUser = User::CheckUserPassword($userId);
            if (password_verify($password, $checkUser['password'])) {
                
                    $result = User::editData($userId, $name, $email, $phone);
            } else {
                $errors[] = 'Ви ввели невірний пароль';
            }

        }

        require_once(ROOT . '/views/cabinet/edit_user_data.php');
        return true;
    }

    /**
     * action user password editing
     * @var string $userId - User ID
     * @var array $user - Array of user information
     * @var string $password - User password
     * @var string $newPass - New user password
     * @var array $errors - Error container
     * @var array $checkUser - Cached user password from database
     * @var boolean $result - Data record status
     * @return bool
     */
    public function actionEditPassword()
    {
        $userId = User::checkLogged();

        $user = User::getUserById($userId);
        
        $password = false;
        $newPass = false;
        $result = false;
        
        if (isset($_POST['submit'])) {
            $password = $_POST['password'];
            $newPass = $_POST['new_password'];

            $errors = false;

            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не повинен бути коротше 6-ти символів';
            }

            $checkUser = User::CheckUserPassword($userId);
            if (password_verify($password, $checkUser['password'])) {
                if (!User::checkPassword($newPass)) {
                    $errors[] = 'Новий пароль не повинен бути коротше 6-ти символів';
                }else{
                    $result = User::editPassword($userId, $newPass);
                }

            } else {
                $errors[] = 'Ви ввели невірний пароль';
            }
        }
        require_once(ROOT . '/views/cabinet/edit_user_password.php');
        return true;
    }

    /**
     * action user shopping list
     * @var string $userId - User ID
     * @var array $user - Array of user information
     * @var array $orders - Array of information orders
     * @var array $productsIds - Array id of ordered products
     * @var array $products - Array of information about the ordered products
     * @var integer $totalPrice - Total price of ordered products
     * @var array $results - Array of information about the ordered products, total price, order status and date of order
     * @return bool
     */
    public function actionHistory()
    {
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        $orders = Order::getOrdersListByEmail($user);
        $results = array();
        foreach ($orders as $order) {
            $productsIds = array_keys($order['products']);
            $products = Product::getProductsByIds($productsIds);

            $totalPrice = Cart::getTotalPrice($products, $order['products']);

            $results[] = [
                'order_products' => $products,
                'total_price' => $totalPrice,
                'order_status' => $order['status'],
                'order_date' => $order['date'],
                'order_count' => $order['products']
            ];
        }


        require_once(ROOT . '/views/cabinet/order_history.php');
        return true;
    }

}