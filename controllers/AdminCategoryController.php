<?php

/**
 * Class AdminCategoryController
 * Extends AdminBase
 * Create, updates and delete product categories
 */
class AdminCategoryController extends AdminBase
{

    /**
     * action category page in the admin panel
     * @var array $categoriesList - Array of categories
     * @return bool
     */
    public function actionIndex()
    {
        self::checkAdmin();

        $categoriesList = Category::getCategoriesListAdmin();

        require_once(ROOT . '/views/admin_category/index.php');
        return true;
    }

    /**
     * action create product categories
     * @var string $name - Name of category
     * @var string $sortOrder - Sequence number of category
     * @var string $status - Whether or not the category is displayed(active)
     * @var array $errors - Error container
     * @return bool
     */
    public function actionCreate()
    {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $sortOrder = $_POST['sort_order'];
            $status = $_POST['status'];

            $errors = false;

            if (!isset($name) || empty($name)) {
                $errors[] = 'Заповніть поля';
            }
            
            if ($errors == false) {
                Category::createCategory($name, $sortOrder, $status);
                
                header("Location: /admin/category/");
            }
        }

        require_once(ROOT . '/views/admin_category/create.php');
        return true;
    }

    /**
     * action update product categories
     * @param string $id - Category ID
     * @var array $category - Category information
     * @var string $name - Name of category
     * @var string $sortOrder - Sequence number of category
     * @var string $status - Whether or not the category is displayed(active)
     * @return bool
     */
    public function actionUpdate($id)
    {
        self::checkAdmin();

        $category = Category::getCategoryById($id);

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $sortOrder = $_POST['sort_order'];
            $status = $_POST['status'];

            Category::updateCategoryById($id, $name, $sortOrder, $status);

            header("Location: /admin/category");
        }

        require_once(ROOT . '/views/admin_category/update.php');
        return true;
    }

    /**
     * action delete product categories
     * @param string $id
     * @return bool
     */
    public function actionDelete($id)
    {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            Category::deleteCategoryById($id);

            header("Location: /admin/category");
        }

        require_once(ROOT . '/views/admin_category/delete.php');
        return true;
    }

}
