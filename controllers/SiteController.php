<?php


/**
 * Class SiteController
 * Home page and contact actions
 */
class SiteController
{

    /**
     * action home page
     * @var array $categoriesList - Categories array
     * @var array $latestProducts - Array of the latest products
     * @var array $sliderProducts - Array of recommended products for the slider
     * @return bool
     */
    public function actionIndex()
    {
        $categoriesList = array();
        $categoriesList = Category::getCategoryList();

        $latestProducts = array();
        $latestProducts = Product::getLatestProductHome(9);

        $sliderProducts = Product::getRecommendedProducts();

        require_once (ROOT . '/views/site/index.php');

        return true;
    }

    /**
     * action to send a message
     * @return bool
     */
    public function actionContact()
    {
        $userEmail = '';
        $userText = '';
        $result = false;
        if(isset($_POST['submit'])){
            $userEmail = $_POST['userEmail'];
            $userText = $_POST['userText'];

            $errors = false;
            if(!User::checkEmail($userEmail)){
                $errors[] = 'Невірний email';
            }
            if($errors == false){
                $bodyText = 'Повідомлення від ' . $userEmail .  PHP_EOL  . $userText;
                $send_to = 'toliktolik@e-shop1.zzz.com.ua';
                $subject = 'Повідомлення від користувача';

                $result = Utf8mail::sendUtf8mail($send_to, $subject, $bodyText);
            }
        }
        require_once (ROOT . '/views/site/contact.php');
        return true;
    }

}