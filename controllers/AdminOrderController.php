<?php

/**
 * Class AdminOrderController
 * Extends AdminBase
 * Editing, viewing and deleting orders
 */
class AdminOrderController extends AdminBase
{

    /**
     * action customer orders page in the admin panel
     * @var array $ordersList - Customer orders
     * @return bool
     */
    public function actionIndex()
    {
        self::checkAdmin();

        $ordersList = Order::getOrdersList();

        require_once(ROOT . '/views/admin_order/index.php');
        return true;
    }

    /**
     * action editing customer order
     * @param string $id - Order ID
     * @var array $order - Order information
     * @var string $userName - User name
     * @var string $userPhone - User phone
     * @var string $userComment - User comment
     * @var string $date - Date and time of order
     * @var string $status - Order status: new, in process, in the process of delivery or closed
     * @return bool
     */
    public function actionUpdate($id)
    {
        self::checkAdmin();

        $order = Order::getOrderById($id);

        if (isset($_POST['submit'])) {
            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];
            $userComment = $_POST['userComment'];
            $date = $_POST['date'];
            $status = $_POST['status'];

            Order::updateOrderById($id, $userName, $userPhone, $userComment, $date, $status);

            header("Location: /admin/order/view/$id");
        }

        require_once(ROOT . '/views/admin_order/update.php');
        return true;
    }

    /**
     * action viewing customer order
     * @param string $id - Order ID
     * @var array $order - Order information
     * @var array $productsQuantity - Array of products quantity
     * @var array $productsIds - Array of products ID
     * @var array $products - Product information
     * @var integer $totalPrice - Total price of goods
     * @return bool
     */
    public function actionView($id)
    {
        self::checkAdmin();

        $totalPrice = 0;

        $order = Order::getOrderById($id);

        $productsQuantity = json_decode($order['products'], true);

        $productsIds = array_keys($productsQuantity);

        $products = Product::getProductsByIds($productsIds);
        
        $totalPrice = Cart::getTotalPrice($products, $productsQuantity);

        require_once(ROOT . '/views/admin_order/view.php');
        return true;
    }

    /**
     * action deleting customer order
     * @param string $id - Order ID
     * @return bool
     */
    public function actionDelete($id)
    {
        self::checkAdmin();

        if (isset($_POST['submit'])) {
            Order::deleteOrderById($id);

            header("Location: /admin/order");
        }

        require_once(ROOT . '/views/admin_order/delete.php');
        return true;
    }

}
