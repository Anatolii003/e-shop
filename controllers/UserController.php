<?php

/**
 * Class UserController
 * Log in, sign up, logout and reset password
 */
class UserController
{

    /**
     * @var string $name
     * @var string $email
     * @var string $password
     * @var string $phone
     * @var array $errors
     * @var bool $result
     * @return bool
     */
    public function actionRegister()
    {
        $name = '';
        $email = '';
        $password = '';
        $phone = '';
        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $phone = $_POST['phone'];


            $errors = array();

            if (User::checkName($name)) {

            } else $errors[] = 'Ім\'я повинно бути більше 3 символів';

            if (User::checkEmail($email)) {

            } else $errors[] = 'Невірний емейл';

            if (User::checkPassword($password)) {

            } else $errors[] = 'Пароль повинен бути довше 6 символів';

            if (User::checkPhone($phone)) {

            } else $errors[] = 'Телефон повинен бути довше 10 символів';

            if (User::checkEmailExist($email)) {
                $errors[] = 'Такий email уже існує';
            }

            if (User::checkNameExist($name)) {
                $errors[] = 'Такий логін уже існує';
            }

            if (empty($errors)) {
                $result = User::register($name, $phone, $email, $password);
            }
        }
        require_once ROOT . '/views/user/register.php';
        return true;
    }

    /**
     * @var string $email
     * @var string $password
     * @var array $errors
     * @var array $userId - array data current user by email
     * @return bool
     */
    public function actionLogin()
    {
        $email = '';
        $password = '';

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = array();

            $userId = User::checkUserData($email);


            if ($userId == false) {
                $errors[] = 'Невірний email';
            } else {
                if (password_verify($password, $userId['password'])) {
                    User::auth($userId['id']);
                    header('Location: /cabinet/');
                } else {
                    $errors[] = 'Невірний пароль';
                }
            }
        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }

    /**
     * @var string $email
     * @var array $errors
     * @var array $userEmail
     * @var bool $result
     * @var integer $code - random number for password reset
     * @return bool
     */
    public function actionReset()
    {
        $email = '';
        $result = false;

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];

            $errors = array();

            $userEmail = Reset::checkUserEmail($email);
            if ($userEmail == false) {
                $errors[] = 'Такий e-mail відсутній';

            } else {
                $code = rand(1000000, 9999999);
                $bodyText = "Ваш код: $code" . PHP_EOL . "Ваш логін: {$userEmail['login']}";
                Utf8mail::sendUtf8mail($userEmail['email'], 'Скидання паролю', $bodyText);
                $result = Reset::editCode($code, $email);
            }
        }
        require_once(ROOT . '/views/user/reset.php');
        return true;
    }

    public
    function actionLogout()
    {
        unset($_SESSION['user']);
        header("Location: /");
    }

    /**
     * @var string $login
     * @var string $password
     * @var array $errors
     * @var string $code
     * @var array $user
     * @var bool $result
     * @return bool
     */
    public
    function actionResetPass()
    {
        $login = '';
        $code = '';
        $password = '';
        $result = false;

        if (isset($_POST['submit'])) {
            $login = $_POST['name'];
            $code = $_POST['code'];
            $password = $_POST['password'];

            $errors = array();

            if (User::checkPassword($password)) {

            } else $errors[] = 'Пароль повинен бути довше 6 символів';

            $user = Reset::checkUser($login, $code);
            if ($user == false) {
                $errors[] = 'Невірний логін або пароль';
            } else {
                $result = Reset::editPassword($password, $login, $code);
            }
        }
        require_once ROOT . '/views/user/reset_pass.php';
        return true;
    }

}